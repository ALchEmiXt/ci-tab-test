<?php

$lang['required'] 			= "Het %s veld is verplicht.";
$lang['isset']				= "Het %s veld mag niet leeg zijn.";
$lang['valid_email']		= "Het %s veld moet een geldg emailadres bevatten.";
$lang['valid_emails'] 		= "Het %s veld moet een geldg emailadres bevatten.";
$lang['valid_url'] 			= "Het %s veld moet een geldige URL bevatten.";
$lang['valid_ip'] 			= "Het %s veld moet een geldig IP bevatten.";
$lang['min_length']			= "Het %s veld moet minimaal %s characters bevatten.";
$lang['max_length']			= "Het %s veld mag maximaal %s characters bevatten.";
$lang['exact_length']		= "Het %s veld meot precies %s characters bevatten.";
$lang['alpha']				= "Het %s veld mag alleen alphabetical characters bevatten.";
$lang['alpha_numeric']		= "Het %s veld mag alleen alpha-numeric characters bevatten.";
$lang['alpha_dash']			= "Het %s veld mag alleen alpha-numeric characters, underscores, en dashes bevatten.";
$lang['numeric']			= "Het %s veld moet een nummer bevatten.";
$lang['is_numeric']			= "Het %s veld moet een nummer bevatten.";
$lang['integer']			= "Het %s veld moet een integer bevatten";
$lang['matches']			= "Het %s veld moet overeenstemmen met het %s veld.";
$lang['is_natural']			= "Het %s veld moet een nummer bevatten.";
$lang['is_natural_no_zero']	= "Het %s veld moet een getal groter dan nul bevatten.";


/* End of file validation_lang.php */
/* Location: ./system/language/english/validation_lang.php */
