<?php
/**
* Authentication Library
*
* @package Authentication
* @category Libraries
* @author Adam Griffiths
* @link http://programmersvoice.com
* @version 1.0.4
* @copyright Adam Griffiths 2009
*
* Auth provides a powerful, lightweight and simple interface for user authentication
*/

$lang['insufficient_privs'] = "U heeft onvoldoende rechten voor deze pagina.";
$lang['username_callback_error'] = "De username is niet gevonden in onze database.";
$lang['reg_username_callback_error'] = "De username is al in gebruik, selecteer een andere username.";
$lang['reg_email_callback_error'] = "De emailadres is al in gebruik, selecteer een ander emailadres.";
$lang['login_details_error'] = "The username and password did not match our records, please try again.";
$lang['max_login_attempts_error'] = "You have exceeded your maximum number of attempted logins, if you have forgotten your password, please consult the lost password form. You will be able to login again in 15 minutes.";
$lang['logout_perms_error'] = "You have been logged out due to a permission error, please login again.";
$lang['check_of_groep'] = "Max groepsaantal lijkt niet op een groep!";
$lang['userlib_email_activation'] = 'Before you can login to your new account you must first verify that you created it. To do this please follow this link:
     %s

     You must do so within the next %s day(s) or your account will be deleted.';

/**
 * Doorbetalers Library:
 */

$lang['In this run, no paymentrecords are kept'] = 'Voor deze wedstrijd worden geen betalingen bijgehouden';
$lang['Number of registrations with payment'] = 'Aantal inschrijvingen via Doorbetalers';
$lang['Payments with run '] = 'Betalingen bij wedstrijd ';
$lang['Amount to collect'] = 'Totaal te ontvangen bedrag';
$lang['Number of payments'] = 'Totaal aantal uit te voeren transacties';
$lang['Number of payments payed'] = 'Aantal transacties voldaan';
$lang['Number of registrations scheduled'] = 'Aantal transacties ingepland';
$lang['Number of registrations waiting on participant'] = 'Aantal transacties wachtend op deelnemer';
$lang['Number of registrations non-recoverable'] = 'Aantal oninbare transacties';


?>
