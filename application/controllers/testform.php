<?php

class Testform extends CI_Controller {

	function index()
	{
		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('testtabform_view');
		}
		else
		{
			$this->load->view('testform_succes');
		}
	}
	
	
	
}
?>