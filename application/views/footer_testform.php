
<!-- Bootstrap core JavaScript                                                                  -->
<!-- ========================================================================================== -->
   <!-- Placed at the end of the document so the pages load faster -->
   <!-- Local dev uncompressed jquery version -->
   <script src="../js/jquery-1.11.3.js"></script>
   <!-- Include all compiled plugins (below), or include individual files as needed -->
   <script src="../js/bootstrap-3.3.5-dist/dist/js/bootstrap.min.js"></script>
   <script src="../js/bootstrap-remote-tabs.js"></script>

   <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
   <script src="../js/bootstrap-3.3.5-dist/assets/js/ie10-viewport-bug-workaround.js"></script>
<!-- ========================================================================================== -->

</body>
</html>