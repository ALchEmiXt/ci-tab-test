<?php $this->load->view('header_testform'); ?>


<ul class="nav nav-tabs" id="uvpbTabs">
	<li class="active"><a data-toggle="tab" href="#home">Home</a></li>
	<li><a data-toggle="tab" href="#form1" data-tab-url="testform">formtesting</a></li>
	<li><a data-toggle="tab" href="#stats">Stats</a></li>
	<li><a data-toggle="tab" href="#admin">Admin</a></li>
</ul>

<!-- The data and views come here -->		
<div class="tab-content" id="all-tabs">

	<div id="home" class="tab-pane fade in active"><h3><?php echo $welcome ?></h3></div>
	
	<div id="form1" class="tab-pane fade"></div>
	<div id="stats" class="tab-pane fade"><h3>Stats of the pages</h3></div>
	<div id="admin" class="tab-pane fade"><h3>Admin stuff</h3></div>
		
</div>


<?php $this->load->view('footer_testform'); ?>
